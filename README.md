# Spring boot with SpringLoad/Gradle Example #

這是 Spring boot 的範例，使用SpringLoad來做hot deploy加速開發

### 使用方式 ###
* 下載
 
```
#!python

 1. 下載
 2. 解壓縮到工作目錄
 3. 切換到解壓縮路徑
```

* 執行程式
  
```
#!java

linux like command - ./gradlew build
Window command - gradlew.bat build
  
編譯成功後接著執行 java -jar ./build/libs/spring-boot-play-0.0.1-SNAPSHOT.jar
```
![螢幕快照 2016-06-23 下午6.36.17.png](https://bitbucket.org/repo/7ak5Ea/images/1751079759-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-06-23%20%E4%B8%8B%E5%8D%886.36.17.png)