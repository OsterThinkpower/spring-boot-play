package com.thinkpower.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPlayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPlayApplication.class, args);
	}
}
