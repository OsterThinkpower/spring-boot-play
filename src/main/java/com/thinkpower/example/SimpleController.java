package com.thinkpower.example;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by oster on 2016/6/23.
 */
@RestController
public class SimpleController {

    @RequestMapping("/")
    public String index() {
        return "This is the root index.";
    }

    @RequestMapping("/somePerson")
    public Person osterObj() {
        Person returnObj = new Person();
        returnObj.setName("Hello world! Oster-Test");
        returnObj.setAge(40);
        returnObj.setCareer("Software Engineer");

        LoggerFactory.getLogger(SimpleController.class).info("This is Obj {}", returnObj);

        return returnObj;
    }

    class Person {
        private String name;
        private Integer age;
        private String career;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getCareer() {
            return career;
        }

        public void setCareer(String career) {
            this.career = career;
        }
    }

}
